FROM openjdk:12-alpine

WORKDIR /usr/local/hypergraphql

ADD hypergraphql-1.0.3-exe.jar ./

USER nobody

ENTRYPOINT ["java", "-Djetty.host=0.0.0.0", "-jar", "/usr/local/hypergraphql/hypergraphql-1.0.3-exe.jar"] 
