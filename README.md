# Hypergraphql Docker Image

Dockerfile defintiion and Image for Hypergraphql

Also includes some example data and a docker-compose stack defintion which loads an example [SKOS taxonomy (UNESCO)](http://skos.um.es/unescothes/)

`docker-compose up` and head over in a borwser to http://localhost:8080/graphiql to start querying.

### Example Query 

```
{
  Concept_GET_BY_ID(uris: ["http://vocabularies.unesco.org/thesaurus/concept1548"]) {
    _id
    prefLabel(lang: "en")
    altLabel
    modified
    scopeNote(lang: "en")
    related {
      _id
      prefLabel(lang: "en")
      scopeNote(lang: "en")
    }
  }
}

```